const express = require("express");
//Khai báo thư viện path
const path = require('path');
const app = express();
const port = 8000;

// Khai báo thư viện static
//app.use(express.static);

// get 
app.get('/', (request, response) => {
    console.log(__dirname)
    response.sendFile(path.join(__dirname + "/views/sample.06restAPI.order.pizza365.v2.0.html"))
});

app.listen(port, (request, response) => {
    console.log(`Đã vào cổng localhost ${port}`)
})